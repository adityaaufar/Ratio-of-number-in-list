# Ratio-of-number-in-list
The ratios of positive numbers, negative numbers, zeros, even and odd to the length of the array


## About
Simple Python file and script that can find out the ratio of  positive numbers, negative numbers, zeros, even and odd to the length of the array.
Apart from the script, it also has the executable format of the script in the dist folder so the input can be customized.
