from NumberRatio import NumberRatio

if __name__ == '__main__':
    flag_stop = False
    num_array = list()
    out_array = list()
    num = input("Enter how many elements you want: ")
    print('Enter numbers in element, press enter to insert each number')
    for i in range(int(num)):
        order = str(i + 1)
        n = input("num " + order + ": ")
        try:
            num_array.append(int(n))
        except:
            flag_stop = True
            break

    if flag_stop == False:
        print('Your Number: ', num_array)
        print('Ratio of positives, negatives, zeros, even and odd respectively: ', NumberRatio(num_array))
    else:
        print('Please enter non-decimal number')

    input("\npress Enter to exit")