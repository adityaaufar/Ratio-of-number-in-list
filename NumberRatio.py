def NumberRatio(num_array):
    out_array = list()
    countpos = 0
    countneg = 0
    countzero = 0
    counteven = 0
    countodd = 0
    failed_flag = False
    length = len(num_array) #Length of array

    #Loop through element of array and count for each category
    for i in num_array:
        if isinstance(i, int): #Only Process if the element is integer
            if i > 0:
                countpos = countpos + 1 #Count Positive Numbers in Array
            elif i < 0:
                countneg = countneg + 1 #Count Negative Numbers in Array
            else:
                countzero = countzero + 1 #Count Zero Numbers in Array
            if i % 2 == 0:
                counteven = counteven + 1 #Count Even Numbers in Array
            else:
                countodd = countodd + 1 #Count Odd Numbers in Array
        else:
            failed_flag = True
            print('Function Failed to perform, Please enter non-decimal number')

            break

    # Appending to array for each category
    if failed_flag == False:
        out_array.append(round((countpos / length), 3))  # Append total Positives devided by length of array
        out_array.append(round((countneg / length), 3))  # Append total Negatives devided by length of array
        out_array.append(round((countzero / length), 3))  # Append total Zeros devided by length of array
        out_array.append(round((counteven / length), 3))  # Append total Even numbers devided by length of array
        out_array.append(round((countodd / length), 3))  # Append total Odd Numbers devided by length of array

    return out_array  # Output as Array consist of ratios of positives, negatives, zeros, even & odd.